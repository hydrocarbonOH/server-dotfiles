set nocompatible
set number relativenumber
set clipboard=unnamedplus
set splitbelow
set splitright
set cursorcolumn
set cursorline
